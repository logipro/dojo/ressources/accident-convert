<?php

namespace Dojo\Tests;

use Dojo\Accidentologie;
use Dojo\DTO\GeoJSonFeatureCollection;
use Dojo\GeoJSonFactory;
use PHPUnit\Framework\TestCase;

class AccidentologieTest extends TestCase
{

    /**
    * L' $expected n'est pas une donnée brute mais est obtenu en effectuant approximativement le meme algorithme
    * que la fonction testée => on s'approche d'un sophisme circulaire (c'est vrai parce que c'est vrai)
    * Cela s'explique parce que la données resultante brute était initialement trop difficile  à construire:
    * une longue chaine au format Json avec une syntaxe compliqué et des données iincertaines.
    * Ce serait à refaire (cad. j'aurai 1 heure de reusinage à y consacrer), je forgerai la longue chaine Json dans
    * laquelle j'extrairai au fur à mesure
    * des iterations reusinage les fonctions permettant de générer les parties très liées aux constantes.
    * ex.: (la constante ACCIDENT_ (qui est une des plus facile, contrairement aux constantes contenant un texte libre
    * plein d'accents))
    * '{"type":"FeatureCollection","features":[{"type":"Feature","id":"ACCIDENT_41350","properties(...)'
    * '{"type":"FeatureCollection","features":[{"type":"Feature","id":"'.$this->getIdAccident().'",","properties(...)'
     */
    public function testTransformer():void
    {
        $csv = <<<EOS
id;Latitude;Longitude;day;month;hour;prediction
41350;45,774;4,813;1;12;2;1
41351;48,875;2,385;1;12;6;0
EOS;
        $factory = new GeoJSonFactory();
        $expected = json_encode(
            new GeoJSonFeatureCollection(
                'FeatureCollection',
                [
                    $factory->makeGeoJSon("41350", 1, 12, 2, [4.813,45.774 ], 1),
                    $factory->makeGeoJSon("41351", 1, 12, 6, [2.385,48.875 ], 0)
                ]
            )
        );
        
        $accidentologie = new Accidentologie();
        $this->assertSame($expected, $accidentologie->transformer($csv));
    }

    public function testTransformerShort():void
    {
        $csv = <<<EOS
id;Latitude;Longitude;day;month;hour;prediction
41350;45,774;4,813;1;12;2;1
41351;48,875;2,385;1;12;6;0
EOS;
        $factory = new GeoJSonFactory();
        $expected = json_encode(
            new GeoJSonFeatureCollection(
                'FeatureCollection',
                [
                    $factory->makeGeoJSonLight("41350", 1, 12, 2, [4.813,45.774 ], 1),
                    $factory->makeGeoJSonLight("41351", 1, 12, 6, [2.385,48.875 ], 0)
                ]
            )
        );
        
        $accidentologie = new Accidentologie(Accidentologie::SHORT_FONCTION);
        $this->assertSame($expected, $accidentologie->transformer($csv));
    }
}
