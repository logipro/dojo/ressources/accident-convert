<?php

namespace Dojo\Tests;

use Dojo\DTO\GeoJSonPropertiesDefault;
use Dojo\DTO\GeoJSonPropertiesLight;
use Dojo\GeoJSonFactory;
use PHPUnit\Framework\TestCase;

class GeoJSonFactoryTest extends TestCase
{
    public function testMakeGeoJSonAccidentPredit():void
    {
        $factory = new GeoJSonFactory('');
        
        $geo = $factory->makeGeoJSon(
            'ACCIDENT_1',
            1,
            12,
            4,
            [4.813, 45.774],
            GeoJSonFactory::ACCIDENT_PREDIT_VALEUR
        );
        $this->assertSame('Feature', $geo->type);
        $this->assertSame('ACCIDENT_1', $geo->id);

        /** @var GeoJSonPropertiesDefault $properties */
        $properties = $geo->properties;
        $this->assertSame(GeoJSonFactory::ACCIDENT_PREDIT_CODE, $properties->code);
        $this->assertSame(GeoJSonFactory::ACCIDENTOLOGIE_DECEMBRE_2020, $properties->type);
        $this->assertSame(GeoJSonFactory::ECHELLE, $properties->echelle);
        $this->assertSame(GeoJSonFactory::POURCENTAGE, $properties->pourcentage);
        $this->assertSame(false, $properties->clignot);
        $this->assertSame(false, $properties->reccurrence_active);
        $this->assertSame(GeoJSonFactory::URL_ACCIDENT_PREDIT, $properties->url_icone);
        $this->assertSame(sprintf(GeoJSonFactory::FORMAT_DATE, 1, 12, 2020, 4), $properties->date_evt_debut);
        $this->assertSame(sprintf(GeoJSonFactory::FORMAT_DATE, 1, 12, 2020, 4), $properties->date);
        $this->assertSame(GeoJSonFactory::SOURCE, $properties->source);
        $this->assertSame(GeoJSonFactory::ACCIDENT_PREDIT_DESCRIPTION, $properties->description);
        $this->assertSame('ACCIDENT_1', $properties->id_repere);
        $this->assertSame('', $properties->date_evt_fin);
        $this->assertSame(GeoJSonFactory::ACCIDENT_PREDIT_TITRE, $properties->titre);
        $this->assertSame('', $properties->code_chantier);
        $this->assertSame(0, $properties->ref_barreau);

        $this->assertSame('Point', $geo->geometry->type);
        $this->assertSame([4.813, 45.774], $geo->geometry->coordinates);
    }

    public function testMakeGeoJSonAccidentNonPredit():void
    {
        $factory = new GeoJSonFactory('');
        $geo = $factory->makeGeoJSon(
            'ACCIDENT_1',
            1,
            12,
            4,
            [4.813, 45.774 ],
            GeoJSonFactory::ACCIDENT_NON_PREDIT_VALEUR
        );
        $this->assertSame('Feature', $geo->type);
        $this->assertSame('ACCIDENT_1', $geo->id);

        /** @var GeoJSonPropertiesDefault $properties */
        $properties = $geo->properties;
        $this->assertSame(GeoJSonFactory::ACCIDENT_NON_PREDIT_CODE, $properties->code);
        $this->assertSame(GeoJSonFactory::ACCIDENTOLOGIE_DECEMBRE_2020, $properties->type);
        $this->assertSame(GeoJSonFactory::ECHELLE, $properties->echelle);
        $this->assertSame(GeoJSonFactory::POURCENTAGE, $properties->pourcentage);
        $this->assertSame(false, $properties->clignot);
        $this->assertSame(false, $properties->reccurrence_active);
        $this->assertSame(GeoJSonFactory::URL_ACCIDENT_NON_PREDIT, $properties->url_icone);
        $this->assertSame(sprintf(GeoJSonFactory::FORMAT_DATE, 1, 12, 2020, 4), $properties->date_evt_debut);
        $this->assertSame(sprintf(GeoJSonFactory::FORMAT_DATE, 1, 12, 2020, 4), $properties->date);
        $this->assertSame(GeoJSonFactory::SOURCE, $properties->source);
        $this->assertSame(GeoJSonFactory::ACCIDENT_NON_PREDIT_DESCRIPTION, $properties->description);
        $this->assertSame('ACCIDENT_1', $properties->id_repere);
        $this->assertSame('', $properties->date_evt_fin);
        $this->assertSame(GeoJSonFactory::ACCIDENT_NON_PREDIT_TITRE, $properties->titre);
        $this->assertSame('', $properties->code_chantier);
        $this->assertSame(0, $properties->ref_barreau);

        $this->assertSame('Point', $geo->geometry->type);
        $this->assertSame([4.813, 45.774 ], $geo->geometry->coordinates);
    }
    public function testMakeGeoJSonLightAccidentPredit():void
    {
        $factory = new GeoJSonFactory('');
        $geo = $factory->makeGeoJSonLight(
            'ACCIDENT_1',
            1,
            12,
            4,
            [4.813, 45.774],
            GeoJSonFactory::ACCIDENT_PREDIT_VALEUR
        );
        $this->assertSame('Feature', $geo->type);
        $this->assertSame('ACCIDENT_1', $geo->id);

        /** @var GeoJSonPropertiesLight $properties */
        $properties = $geo->properties;
        $this->assertSame(GeoJSonFactory::ACCIDENT_PREDIT_TITRE, $properties->titre);
        $this->assertSame(GeoJSonFactory::ACCIDENT_PREDIT_CODE, $properties->code);
        $this->assertSame(GeoJSonFactory::ACCIDENTOLOGIE_PREDIT, $properties->type);
        $this->assertSame(GeoJSonFactory::ECHELLE, $properties->echelle);
        $this->assertSame(GeoJSonFactory::URL_ACCIDENT_PREDIT, $properties->url_icone);
        $this->assertSame(sprintf(GeoJSonFactory::FORMAT_DATE, 1, 12, 2020, 4), $properties->date);
        $this->assertSame(GeoJSonFactory::SOURCE, $properties->source);

        $this->assertSame('Point', $geo->geometry->type);
        $this->assertSame([4.813, 45.774], $geo->geometry->coordinates);
    }
    public function testMakeGeoJSonLightAccidentNonPredit():void
    {
        $factory = new GeoJSonFactory('');
        $geo = $factory->makeGeoJSonLight(
            'ACCIDENT_1',
            1,
            12,
            4,
            [4.813, 45.774],
            GeoJSonFactory::ACCIDENT_NON_PREDIT_VALEUR
        );
        $this->assertSame('Feature', $geo->type);
        $this->assertSame('ACCIDENT_1', $geo->id);

        /** @var GeoJSonPropertiesLight $properties */
        $properties = $geo->properties;
        $this->assertSame(GeoJSonFactory::ACCIDENT_NON_PREDIT_TITRE, $properties->titre);
        $this->assertSame(GeoJSonFactory::ACCIDENT_NON_PREDIT_CODE, $properties->code);
        $this->assertSame(GeoJSonFactory::ACCIDENTOLOGIE_NON_PREDIT, $properties->type);
        $this->assertSame(GeoJSonFactory::ECHELLE, $properties->echelle);
        $this->assertSame(GeoJSonFactory::URL_ACCIDENT_NON_PREDIT, $properties->url_icone);
        $this->assertSame(sprintf(GeoJSonFactory::FORMAT_DATE, 1, 12, 2020, 4), $properties->date);
        $this->assertSame(GeoJSonFactory::SOURCE, $properties->source);

        $this->assertSame('Point', $geo->geometry->type);
        $this->assertSame([4.813, 45.774], $geo->geometry->coordinates);
    }
}
