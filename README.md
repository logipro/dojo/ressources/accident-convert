Conversion des accidents de décembre 2020 au format json

# Installation

```bash
git clone git@gitlab.com:logipro/dojo/ressources/accident-convert.git
cd accident-convert
bin/composer install
```

# Lancement
Pour générer le fichier json à partir du fichier "AccidentDataDecembre2020.csv" présent à la racine du projet.

```bash
bin/php run.php
bin/php run.php --short
```

Après lancement, le fichier "AccidentDataDecembre2020.json" sera généré. Il se trouvera lui aussi à la racine du projet.

# Tests

Les tests unitaires
```bash
bin/phpunit
```

Vérifications du typage, de la syntaxe, du passage des tests unitaires.

```bash
./codecheck
```