<?php

namespace Dojo;

require_once(__DIR__.'/vendor/autoload.php');

$csv = (string)file_get_contents(__DIR__.'/AccidentDataDecembre2020.csv');


$options = getopt('',['short']);

if (isset($options['short'])) {
    $accidentologie = new Accidentologie(Accidentologie::SHORT_FONCTION);
} else {
    $accidentologie = new Accidentologie();
}
$json = $accidentologie->transformer($csv);

$fp = fopen(__DIR__.'/AccidentDataDecembre2020.geojson', 'w');
fwrite($fp, $json);
fclose($fp);
