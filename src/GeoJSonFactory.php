<?php

namespace Dojo;

use Dojo\DTO\GeoJSon;
use Dojo\DTO\GeoJSonGeometry;
use Dojo\DTO\GeoJSonPropertiesDefault;
use Dojo\DTO\GeoJSonPropertiesLight;

class GeoJSonFactory
{
    public const FEATURE = 'Feature';
    public const PREFIX_ID = 'ACCIDENT_';

    public const ACCIDENT_PREDIT_CODE = 'AccidentPredit';
    public const ACCIDENT_NON_PREDIT_CODE = 'Accident';

    public const ACCIDENTOLOGIE_DECEMBRE_2020 = 'ACCIDENT122020';
    public const ACCIDENTOLOGIE_PREDIT = 'ACCIDENT_PREDIT';
    public const ACCIDENTOLOGIE_NON_PREDIT = 'ACCIDENT_NON_PREDIT';

    public const URL_ACCIDENT_PREDIT = '../swf/repere/accident-predit.svg';
    public const URL_ACCIDENT_NON_PREDIT = '../swf/repere/accident.svg';

    public const ECHELLE = 75;
    public const POURCENTAGE = 100;

    public const SOURCE = 'Labo. Logipro';

    public const FORMAT_DATE = "%02d/%02d/%04d %02dh";

    public const ACCIDENT_NON_PREDIT_DESCRIPTION = 'Cet accident n\' a pu être prédit.';
    public const ACCIDENT_PREDIT_DESCRIPTION= 'Prédit moins de 60 minutes à l\'avance';

    public const ACCIDENT_PREDIT_TITRE = 'Accident prédit';
    public const ACCIDENT_NON_PREDIT_TITRE = 'Accident';

    public const ACCIDENT_PREDIT_VALEUR = 1;
    public const ACCIDENT_NON_PREDIT_VALEUR = 0;

    public function __construct(private string $prefixId = self::PREFIX_ID)
    {
    }

    /**
     * @param array<int,double> $coordinates
     */
    public function makeGeoJSon(
        string $id,
        int $day,
        int $month,
        int $hour,
        array $coordinates,
        int $prediction
    ): GeoJSon {
        $geoJSonId = $this->prefixId.$id;
        $geo = new GeoJSon(
            self::FEATURE,
            $geoJSonId,
            $this->makeGeoJSonProperties($day, $month, $hour, $prediction, $geoJSonId),
            $this->makeGeoJSonGeometry($coordinates)
        );
        return $geo;
    }

    private function makeGeoJSonProperties(
        int $day,
        int $month,
        int $hour,
        int $prediction,
        string $geoJSonId
    ):GeoJSonPropertiesDefault {
        return new GeoJSonPropertiesDefault(
            $this->getCode($prediction),
            self::ACCIDENTOLOGIE_DECEMBRE_2020,
            self::ECHELLE,
            self::POURCENTAGE,
            false,
            false,
            $this->getUrlPictogramme($prediction),
            $this->getDateEvtDebut($day, $month, $hour),
            $this->getDate($day, $month, $hour),
            self::SOURCE,
            $this->getDescription($prediction),
            $geoJSonId,
            '',
            $this->getTitre($prediction),
            '',
            0
        );
    }

    /**
     * @param array<int,double> $coordinates
     */
    private function makeGeoJSonGeometry(array $coordinates):GeoJSonGeometry
    {
        return new GeoJSonGeometry(
            'Point',
            $coordinates
        );
    }

    private function getCode(int $prediction):string
    {
        return $prediction == self::ACCIDENT_PREDIT_VALEUR ?
        self::ACCIDENT_PREDIT_CODE : self::ACCIDENT_NON_PREDIT_CODE;
    }

    private function getType(int $prediction):string
    {
        return $prediction == self::ACCIDENT_PREDIT_VALEUR ?
        self::ACCIDENTOLOGIE_PREDIT : self::ACCIDENTOLOGIE_NON_PREDIT;
    }
    private function getUrlPictogramme(int $prediction):string
    {
        return $prediction == self::ACCIDENT_PREDIT_VALEUR ? self::URL_ACCIDENT_PREDIT : self::URL_ACCIDENT_NON_PREDIT;
    }

    private function getDateEvtDebut(int $day, int $month, int $hour):string
    {
        return sprintf(self::FORMAT_DATE, $day, $month, 2020, $hour);
    }

    private function getDate(int $day, int $month, int $hour):string
    {
        return sprintf(self::FORMAT_DATE, $day, $month, 2020, $hour);
    }

    private function getDescription(int $prediction):string
    {
        return $prediction == self::ACCIDENT_PREDIT_VALEUR ?
        self::ACCIDENT_PREDIT_DESCRIPTION : self::ACCIDENT_NON_PREDIT_DESCRIPTION;
    }

    private function getTitre(int $prediction):string
    {
        return $prediction == self::ACCIDENT_PREDIT_VALEUR ?
        self::ACCIDENT_PREDIT_TITRE : self::ACCIDENT_NON_PREDIT_TITRE;
    }

    /**
     * @param array<int,double> $coordinates
     */
    public function makeGeoJSonLight(
        string $id,
        int $day,
        int $month,
        int $hour,
        array $coordinates,
        int $prediction
    ): GeoJSon {
        $geoJSonId = $this->prefixId.$id;
        $geo = new GeoJSon(
            self::FEATURE,
            $geoJSonId,
            $this->makeGeoJSonPropertiesLight($day, $month, $hour, $prediction),
            $this->makeGeoJSonGeometry($coordinates)
        );
        return $geo;
    }

    private function makeGeoJSonPropertiesLight(
        int $day,
        int $month,
        int $hour,
        int $prediction
    ):GeoJSonPropertiesLight {
        return new GeoJSonPropertiesLight(
            $this->getTitre($prediction),
            $this->getCode($prediction),
            $this->getType($prediction),
            self::ECHELLE,
            $this->getUrlPictogramme($prediction),
            $this->getDate($day, $month, $hour),
            self::SOURCE
        );
    }
}
