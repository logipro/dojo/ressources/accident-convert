<?php

namespace Dojo;

use Dojo\DTO\GeoJSon;
use Dojo\DTO\GeoJSonFeatureCollection;

class Accidentologie
{
    public const SHORT_FONCTION = 1;
    public const DEFAULT_FONCTION = 2;

    public string $functionGeoJson = 'makeGeoJSon';
    public function __construct(int $function = self::DEFAULT_FONCTION)
    {
        if ($function == self::SHORT_FONCTION) {
            $this->functionGeoJson = 'makeGeoJSonLight';
        }
    }

    public function transformer(string $csvAccidents):string
    {
        $csv = $this->transformCsvStringToArray($csvAccidents);
        $geojsons = $this->buildGeoJSons($csv);
        return $this->encodeGeoJsonFeatureCollectionToJSon($geojsons);
    }

    /**
     * @return array<int,array<string,string>> $csv
     */
    private function transformCsvStringToArray(string $csvAccidents):array
    {
        $csv = explode("\n", $csvAccidents);
        $entete = str_getcsv($csv[0], ";");
        $result = [];
        array_walk($csv, function (&$a) use ($entete, &$result) {
            $ligne = str_getcsv($a, ";");
            $result[] = array_combine($entete, $ligne);
        });
        array_shift($result); # remove column header
        return $result;
    }

    /**
     * @param array<int,array<string,string>> $csv
     * @return array<int,GeoJSon>
     */
    private function buildGeoJSons(array $csv):array
    {
        $factory = new GeoJSonFactory();
        $geojsons = [];
        $funName = $this->functionGeoJson;
        foreach ($csv as $ligne) {
            $geojsons[] = $factory->$funName(
                $ligne['id'],
                (int)$ligne['day'],
                (int)$ligne['month'],
                (int)$ligne['hour'],
                [(double)str_replace(',', '.', $ligne['Longitude']),(double)str_replace(',', '.', $ligne['Latitude'])],
                (int)$ligne['prediction']
            );
        }
        return $geojsons;
    }

    /**
     *  @param array<int,GeoJSon> $geojsons
     */
    private function encodeGeoJsonFeatureCollectionToJSon(array $geojsons):string
    {
        return (string)json_encode(
            new GeoJSonFeatureCollection(
                'FeatureCollection',
                $geojsons
            )
        );
    }
}
