<?php

namespace Dojo\DTO;

class GeoJSonPropertiesDefault extends GeoJSonPropertiesLight
{
    public function __construct(
        public string $code,
        public string $type,
        public int $echelle,
        public int $pourcentage,
        public bool $clignot,
        public bool $reccurrence_active, // deprecated
        public string $url_icone, //erreur
        public string $date_evt_debut,
        public string $date,
        public string $source,
        public string $description,
        public string $id_repere,
        public string $date_evt_fin, // ajouts
        public string $titre,
        public string $code_chantier,
        public int $ref_barreau
    ) {
    }
}
