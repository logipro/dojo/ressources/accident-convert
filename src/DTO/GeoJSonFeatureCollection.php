<?php

namespace Dojo\DTO;

class GeoJSonFeatureCollection
{
    /**
     * @param string $type
     * @param array<int,GeoJSon> $features
     */
    public function __construct(
        public string $type,
        public array $features
    ) {
    }
}
