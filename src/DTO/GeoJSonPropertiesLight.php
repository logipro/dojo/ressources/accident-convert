<?php

namespace Dojo\DTO;

class GeoJSonPropertiesLight extends GeoJSonProperties
{
    public function __construct(
        public string $titre,
        public string $code,
        public string $type,
        public int $echelle,
        public string $url_icone, //erreur
        public string $date,
        public string $source
    ) {
    }
}
