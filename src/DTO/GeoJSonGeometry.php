<?php

namespace Dojo\DTO;

class GeoJSonGeometry
{
    /**
     * @param array<int,double> $coordinates
     */
    public function __construct(
        public string $type,
        public array $coordinates
    ) {
    }
}
