<?php

namespace Dojo\DTO;

class GeoJSon
{
    public function __construct(
        public string $type,
        public string $id,
        public GeoJSonProperties $properties,
        public GeoJSonGeometry $geometry
    ) {
    }
}
